---
title: "CKAD Multi Container Pods"
date: 2020-04-10T11:01:07+08:00
draft: false

categories: [kubernetes, CKAD]
tags: [kubernetes, CKAD]
---

Materi kali ini saya akan bahas tentang Multi-Container-pods (10%)

Multi kontainer pods porsi nya ada 10% dari total ujian CKAD, jadi cukup penting juga untuk di kuasai.
materi yang di ujiankan berdassarkan materi ini adalah:

- mengerti design multi kontainer pod pattern (ambassador, adaptor dan sidecar)

1. buat sebuah pod dengan 3 busybox kontainer, kontainer 1 jalankan `ls` lalu `sleep 3600`, kontainer 2 jalankan `echo Hello World` lalu `sleep 3600`, kontainer 3 jalankan `echo this third container`, lalu `sleep 3600`

```kubectl 
// pertama kita buat pod dengan satu kontainer dengan opsi --dry-run suapa tidak di eksekusi oleh kubectl, lalu kita save ke file dengan nama multi-kontainer-pod.yaml

kubectl run busybox --image=busybox --restart=Never --dry-run -o yaml -- /bin/sh -c "ls; sleep 3600" > multi-container-pod.yaml

// edit file multi-container-pod.yaml, opsi saat ujian adalan pakai vi, vim atau nano. 

vim multi-container-pod.yaml

// save dan jalan kubectl create -f multi-container-pod.yaml
kubectl create -f multi-container-pod.yaml
```

isi file multi-container-pod.yaml harus seperti berikut ini :

```yaml 
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: busybox
  name: busybox
spec:
  containers:
  - args:
    - bin/sh
    - -c
    - ls; sleep 3600
    image: busybox
    name: busybox1
    resources: {}
  - args:
    - bin/sh
    - -c
    - echo Hello world; sleep 3600
    image: busybox
    name: busybox2
    resources: {}
  - args:
    - bin/sh
    - -c
    - echo this is third container; sleep 3600
    image: busybox
    name: busybox3
```

2. cek log tiap container yang ada di dalam pod busybox

```kubectl
kubectl logs busybox -c busybox1
kubectl logs busybox -c busybox2
kubectl logs busybox -c busybox3
```

3. cek previous log dari container busybox2

```kubectl
kubectl logs busybox -c busybox2 --previous
```

4. jalan kan perintah `ls` di container busybox3
    
```kubectl
kubectl exec busybox -c busybox3 -- ls
```

5. tampilkan metrics pod busybox dan simpan dalam file lalu verify
    
```kubectl
kubectl top pod busybox --containers

// simpan dalam file
kubectl top pod busybox --containers > file.log
cat file.log
```

6. buat sebuah pod dengan container busybox, jalankan perintah `while true; do echo 'Hi I am from Main container' >> /var/log/index.html; sleep 5; done` dengan sidecar container nginx dan expose di port 80. Gunakan emptyDir volume dan mount volume ini di /var/log untuk busybox dan /usr/share/nginx/html untuk nginx container. pastikan container running.

```kubectl
// buat inisial yaml file 
kubectl run multi-cont-pod --image=busybox --restart=Never --dry-run -o yaml > multi-container-pod.yaml

// edit file yaml dan eksekusi kubectl create -f multi-container-pod.yaml
kubectl create -f multi-container-pod.yaml

kubectl get po multi-cont-pod
```

isi file multi-container-pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: multi-cont-pod
  name: multi-cont-pod
spec:
  volumes:
  - name: var-logs
    emptyDir: {}
  containers:
  - image: busybox
    command: ["/bin/sh"]
    args: ["-c", "while true; do echo 'Hi I am from Main container' >> /var/log/index.html; sleep 5;done"]
    name: main-container
    resources: {}
    volumeMounts:
    - name: var-logs
      mountPath: /var/log
  - image: nginx
    name: sidecar-container
    resources: {}
    ports:
      - containerPort: 80
    volumeMounts:
    - name: var-logs
      mountPath: /usr/share/nginx/html
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}
```

7. exec ke dalam 2 container tersebut untuk ngecek kalao file yg diminta exist dan query the main container dari sidecar container dengan curl localhost

```kubectl
// exec into main container

kubectl exec -it  multi-cont-pod -c main-container -- sh
cat /var/log/index.html

// exec into sidecar container

kubectl exec -it  multi-cont-pod -c sidecar-container -- sh
cat /usr/share/nginx/html/index.html

// install curl and get default page

kubectl exec -it  multi-cont-pod -c sidecar-container -- sh

apt-get update && apt-get install -y curl
curl localhost

```

latihan yang lebih di soal no 6 dan pastikan baca soalnya dengan baik. 
latihannya dengan cara bikin multi container, latihan untuk masukkan perintah-perintah sederhana dengan shell script dan baca lagi sharing volume di pods. 

perlu untuk latihan kubectl explain pods untuk ngelihat skema lengkap sebuah pod untuk kita jadikan rujukan.

