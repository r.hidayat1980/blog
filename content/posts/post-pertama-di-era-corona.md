---
title: "Post pertama di era corona!"
date: 2020-04-09T18:01:27+08:00
draft: false
categories: [unknown]
tags: [unknown, hello word]
---

Karena lagi bosen banget saya akhirnya iseng-iseng nulis dan ngeblog lagi, sekalian mau coba hugo dan liat-liat hostingan buat static site generator yang gratis, SSL juga gratis dan mudah di gunakan. saya coba pakai gitlab pages, github pages tapi rasanya kurang nyaman dan ada limit bandwidth nya :(

di blog ini rencananya mau saya tulis semua aktifitas saya selama masa lockdown di kualalumpur, yang kemungkinan bakalan diperpanjang sampai lebaran idul fitri nanti. sudah kebayang dah gimana bosennya nanti, mana anak istri lagi di jakarta pula. nah dengan alasan-alasan itulah saya coba untuk ngilangin rasa bosen dengan cara ngeset target untuk ujian certified `kubernetes for application developer (CKAD)`, `certified kubernetes administrator (CKA)` dan juga `GCP cloud professional` dan juga semua proses nya bakalan saya tulis disini.

seperti yg sudah saya ceritaken di atas kalau blog ini di buat pakai `hugo` static site generator, reponya di host di `gitlab.com` dan hasil generated files nya di host di `onrender.com`, buat kalian yg mau ikut cobain onrender monggo loh ya.

untuk feature silahkan liat sendiri, berdasarkan perbandingan singkat dengan github dan gitlab pages menurut saya onrender.com ini jauh lebih baik.