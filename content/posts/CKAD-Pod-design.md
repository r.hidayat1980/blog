---
title: "CKAD Pod Design"
date: 2020-04-10T11:03:07+08:00
draft: false
categories: [kubernetes, CKAD]
tags: [kubernetes, CKAD]
---

Materi Pod Design (20%).

praktek pods design:

- Understand how to use Labels, Selectors and Annotations.
- Understand Deployments and how to perform rolling update.
- Understand Deployments and how to perform rollback.
- Understand Jobs and CronJobs.

Materi Pod Design ini adalah salah satu materi yang cukup besar menurut saya, karena meng cover metadata sebuah object seperti Labels, selector dan annotations, juga ada deployments dan behavior yg bisa kita buat untuk si deployments object ini. dan rata-rata contoh yang di berikan juga via on the fly untuk rolling update dan roll back bukan dengan cara edit file deployment. menurut saya mungkin demi mempercepat waktu saja.

## 1. Get the pods with label information

```kubectl
kubectl get pods --show-labels
```

## 2. Create 5 nginx pods in which two of them is labeled env=prod and three of them labeled env=dev

```kubectl
kubectl run nginx-prod1 --image=nginx --restart=Never --labels=env=prod
kubectl run nginx-prod2 --image=nginx --restart=Never --labels=env=prod

kubectl run nginx-dev1 --image=nginx --restart=Never --labels=env=dev
kubectl run nginx-dev2 --image=nginx --restart=Never --labels=env=dev
kubectl run nginx-dev3 --image=nginx --restart=Never --labels=env=dev
```

## 3. verify all the pods are created with correct labels

```kubectl
kubectl get pod --show-labels
```

## 4. get the pods with labels env=dev

```kubectl
kubectl get pods -l env=dev
```

## 5. get the pods with label env=dev and also output the labels

```kubectl
kubectl get pods -l env=dev --show-labels
```

## 6. get the pods with labels env=prod

```kubectl
kubectl get pods -l env=prod
```

## 7. get the pods with labels env=prod and also output the labels

```kubectl
kubectl get pods -l env=prod --show-labels
```

## 8. get the pods with labels env=dev and env=prod

```kubectl
kubectl get pods -l 'env in (dev,prod)'
```

## 9. get the pods with labels env=dev and env=prod and output the labels as well

```kubectl
kubectl get pods -l 'env in (dev,prod)' --show-labels
```

## 10. change the label for one of the pod to env=uat and list all the pods to verify

```kubectl
kubectl label pod/nginx-dev3 env=uat --overwrite
kubectl get pods --show-labels
```

## 11. Remove the labels for the pods that we created now and verify all the labels are removed

```kubectl
kubectl label pod/nginx-dev{1..3} env-
kubectl label pod/nginx-prod{1..2} env-
kubectl get pods --show-labels
```
