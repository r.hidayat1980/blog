---
title: "CKAD Core Concepts"
date: 2020-04-10T11:01:07+08:00
draft: false

categories: [kubernetes, CKAD]
tags: [kubernetes, CKAD]
---

Core concepts (13%)

Practise questions based on these concepts:

- Understand Kubernetes API Primitives
- Create ANd configure Basic Pods

Tulisan kali ini saya akan membahas materi core concepts untuk ujian CKAD.

konsep utama di kubernetes adalah memahami arsitektur dari kubernetes itu sendiri. Kubernetes cluster di bangun dari dua komponen yaitu komponen master dan worker(nodes/minions) dan di masing-masing komponen ini juga di susun lagi oleh komponen-komponen kecil.

k8s master terdiri dari:

- api server (frontend nya kubernetes, semua perintah kubectl berhubungan langsung dengan api server)
- etcd data store (semua data beruba identity, kondisi, size, configurasi dari semuah obejct manifest di simpan di sini)
- scheduler (seperti namanya ini untuk mendengarkan perintah dari controller dan melakukan komunikasi dengan kubelet)
- controller (otak dari kubernetes yang memerintahkan ke komponen lain untuk melakuka perintah)

k8s worker nodes terdiri dari:

- container runtime (dalam hal ini docker)
- kubelet (sebagai networking dan bridge k8s master ke docker runtime)

Di materi core concepts ini kita di minta untuk ngerti komponen terkecil dari kubernetes yaitu pod. pod adalah lingkungan yang akan menjalankan container di worker nodes. semua container yg berada dalam satu pod secara otomatis akan sharing network, proses dan juga volumes (direktori dan file system).

buat yang penasaran dengan berapa banyak default object yang ada di kubernetes bisa melihat dengan menjalankan perintah berikut:

```kubectl
kubectl api-server
```

Materi core concepts kita diminta untuk ngerti apa itu `namespaces`, `pods`, mengelola pod secara runtime dengan perintah `kubectl run`, mengelola pod secara deskriptif dengan perintah `kubectl create` atau `kubectl apply` lalu diminta untuk coba jalanin pod di namespace tertentu yang diminta.

perintah-perintah runtime tidak membutuhkan input yaml atau json file, sedangkan perintah deskriptif memerlukan input file yaml atau jason.

kalau teman-teman bingung dengan dua tipe file ini, sebetulnya kubernetes hanya menerima json file, `namun dibalik layar si kubectl command ini melakukan transformasi dari yaml ke json`. 

untuk membedakan standar output ketika debugging kubernetes object kita bisa ngasih flag `-o yaml` atau `-o json`, ini untuk melihat output dalam bentuk yaml atau json. misalnya:

```kubectl
kubectl get pod nginx -o yaml 
kubectl get pod nginx -o json 
```

1. buat namespace nyx dan deploy pod busybox di namespaces nyx

```kubectl
kubectl create ns nyx
kubectl run busybox --image=busybox --restart=Never --namespace=nyx
```

2. list semua namespace di clusters

```kubectl
kubectl get namespaces
kubectl get ns
```

3. list semua pods di semua namespace

```kubectl
kubectl get po --all-namespaces
```

4. list semua pods di namespace tertentu

```kubectl
kubectl get po -n nyx
```

5. list semua service di namespace tertentu

```kubectl
kubectl get svc -n nyx
```

6. list semua pods dengan name dan namespace nya deingan jsonpath

```kubectl
kubectl get pods -o=jsonpath="{.items[*]'metadata.name','metadata.namespace'}"
```

7. buat pod nginx di namespace default dan verify pod nginx tersebut running dengan baik

```kubectl
kubectl run nginx --image=nginx --restart=Never
kubectl get pods 
```

8. buat pod nginx yang sama tapi kali ini dengan menggunakan yaml file (cara deklaratif)

```kubectl
kubectl run nginx --image=nginx --restart=Never --dry-run -o yaml > nginx-pod.yaml

// cat nginx-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx
spec:
  containers:
  - image: nginx
    name: nginx
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
status: {}

// create pod based on this yaml file
kubectl create -f nginx-pod.yaml
```
jika tanpa namespace flag maka otomatis akan ke namespace default.

9. cek hasil deploy pod nginx, output harus yaml 

```kubectl
kubectl get po nginx -o yaml 
```

10. print yaml file dari pod nginx tanpa cluster specific information. 

```kubectl
kubectl get po nginx -o yaml --export
```

11. print informasi detail dari pod nginx

```kubectl
kubectl describe pod nginx
```

12. delete pod nginx

```kubectl
kubectl delete po nginx 

// atau bisa juga denga cara
kubectl delete -f nginx-pod.yaml
```

13. delete pod tanpa delay 

```kubectl
kubectl delete po nginx --grace-period=0 --force
```

14. buat pod nginx dengan versi nginx 1.17.4 dan expose di port 80 

```kubectl
kubectl run nginx --image=nginx:1.17.4 --restart=Never --port=80
```

15. ubah versi image nginx ke 1.15-alpine pada pod nginx tadi, lalu verify
    
```kubectl
kubectl set image pod/nginx nginx=1.15-alpine
kubectl describe po nginx 

// cara lainnya dengan langung mengubah dengan kubectl edit
kubectl edit pod nginx

// verify pod nginx
kubectl describe po nginx
```

16. ubah balik versi nginx nya ke 1.17.1 dan cek hasilnya

```kubectl 
kubectl set image pod/nginx nginx=1.17.1
kubectl describe po nginx

// -w untuk watch
kubectl get po nginx -w  
```

17. check versi tanpa kubectl describe

```kubectl
kubectl get po nginx -o jsonpath='{.spec.containers[].image}{"\n"}'
```

18  buat pod nginx dan jalankan simple shell script di pod tersebut

```kubectl
kubectl run nginx --image=nginx --restart=Never

// exec ke dalam pod nginx
kubectl exec -it nginx /bin/sh
```

19. dapatkan info IP address pod nginx

```kubectl
kubectl get po nginx -o wide
```

20. buat busybox pod dan jalankan perintah `ls` dan check log nya
    
```kubectl
kubectl run busybox --image=busybox --restart=Never --ls 
kubectl logs busybox
```

21. cara cek log jika pod yang di deploy crashed

```kubectl
kubectl logs busybox -p
```

22. buat pod busybox dan masukkan perintah sleep 3600 
    
```kubectl
kubectl run busybox --image=busybox --restart=Never -- /bin/sh -c "sleep 3600"
```

23. cek koneksi nginx dari pod busybox
    
```kubectl
// dapat kan ip pod nginx
kubectl get po nginx -o wide 

// akses pod busybox
// ganti ip nginx dengan hasil perintah diatas
kubectl exec -it busybox -- wget -0- ip-nginx
```

24. buat pod busybox dan jalankan perintah echo "How are you" lalu delete pod itu manually

```kubectl
kubectl run busybox --image=busybox --restart=Never -it -- echo "How are you"

kubectl delete pod busybox
```

25. buat pod busybox dan jalankan perintah echo "How are you" lalu delete pod itu otomatis

```kubectl
kubectl run busybox --image=busybox --restart=Never -it --rm -- echo "How are you"
```

26. buat pod nginx dan list pod tersebut dengan level verbosity yg berbeda.

```kubectl
kubectl run nginx --image=nginx --restart=Never --port=80 

// list pod nginx dengan verbosity yg berbeda 
kubectl get po nginx --v=7
kubectl get po nginx --v=8 
kubectl get po nginx --v=9
```

27. list pod nginx dengan custom kolom POD_NAME dan POD_STATUS
    
```kubectl
kubectl get po -o=custom-columns="POD_NAME:.metadata.name, POD_STATUS:.status.containersStatuses[].state"
```

untuk tau formatnya kamu bisa pakai `kubectl explain pod --recursive`

28. list semua pod based on namanya

```kubectl
kubectl get pods --sort-by=.metadata.name
```

29. list semua pods based on created timestamp

```kubectl 
kubectl get pods --sort-by=.metadata.creationTimestamp
```

rangkuman dari k8s core concept:

- harus tau apa itu namespace dan cara buat nya
- ngerti apa itu pod, cara buat nya dengan cara on the fly dan deskriptif
- cara generate yaml file dengan menambahkan flag --dry-run tiap kali mau membuat pods dengan alasan lebih mudah dan ada record yg bisa kita manipulasi nantinya.
- mastering kubectl explain `k8s object`
- mastering kubectl help `k8s-object`
- mastering kubectl describe `k8s-object`
- mastering kubectl run 
- mastering kubectl create k8s-object 
- mastering cara melihat skema setiap `k8s-object` dengan cara `kubectl exmplain pods` misalnya
- untuk ngelist semua k8s-object bisa dengan cara `kubectl api-servers`


kalau saya bilang k8s-object itu bisa pods, deployment, statefulsets, service, ingress, configmap dll.

Kualalumpur, 11 april 2020