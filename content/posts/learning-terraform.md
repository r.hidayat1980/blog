---
title: "Learning Terraform"
date: 2020-06-04T07:53:50+08:00
draft: false
categories: [terraform, devops]
tags: [terraform, devops]
---

## Terraform intro

Terraform adalah sebuah automation tools yang biasa di sebut infrastructure as code (Iac) di gunakan untuk meng automasisasi cloud provider, biasanya dipakai oleh orang-orang devops, cloud engineer bahkan oleh developer untuk membantu dalam menyediakan environment untuk aplikasi mereka di Cloud.

Terraform dari sisi produk adalah sebuah produk dari haschicorp. dibuat dengan bahasa Golang, karena itu instalasi terraform sangatlah mudah, single binary win!

## Instalasi terraform

untuk instalasi terraform sendiri sangatlah mudah, karena terraform adalah single binary, jadi kamu tinggal download sesuai dengan sistem operasi yang akan kamu gunakan ketika menjalankan perintah terraform.

disini saya menggunakan sebuah tools yang bernama `asdf`. asdf adalah tools yang saya gunakan untuk membantu menginstall multiple version of application/programming language/tools dll.

dokumentasi asdf bisa kamu lihat di sini:
<https://asdf-vm.com/#/>

Untuk MacOS

```bash
brew update
brew install coreutils curl git
brew install asdf
asdf plugin-add terraform
asdf install terraform 0.12.26 && asdf global terraform 0.12.26
terraform version
```

Untuk Ubuntu

```bash
sudo apt-get install git
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.8

vim ~/.bashrc

// tambahkan baris ini

. $HOME/.asdf/asdf.sh

// exit vim
source ~/.bashrc

asdf update
asdf plugin-add terraform
asdf install terraform 0.12.26 && asdf global terraform 0.12.26
terraform version
```

Untuk tutorial asdf akan saya buatkan terpisah nantinya. Kenapa saya anggap asdf penting karena nanti akan sangat berguna ketika kita akan menginstall banyak applikasi di sebuah CI/CD system, atau bayangkan ketika kita akan menjalankan terraform dari sebuah docker container. Diakhir tulisan ini saya akan demoin bagaimana menggunakan terraform di dalam docker container juga bagaimana menggunakan terraform di CI/CD seperti Jenkins ataupun Gitlab.

## terraform user to access AWS (programmatic)

Dalam tulisan ini saya berasumsi Cloud provider yang akan kita gunakan adalah AWS. oleh karena itu setelah instalasi terraform kita juga membutuhkan sebuah provider dalam hal ini AWS provider. Dalam tulisan ini juga saya akan mulai dengan hal sederhana seperti struktur terraform projek yang hanya 1 file (flat) yang akan bersis data, resources dan variables.

sebelum memulai pastikan kalian sudah punya akses ke AWS console. buat sebuah user untuk terraform dengan memberi akses agar user ini punya akses ke resources yang akan kita buat, misalnya VPC, EC2, RDS dan sebagainya. asumsinya kita berinama `terraform-user`, pastikan juga hanya programatic access. Kalau kalian tidak yakin dengan access level, cukup kasih administrator access ke `terraform-user`. saya akan tulis juga panduan soal IAM untuk AWS nantinya. klik Create user yang nantinya akan membuat access key ID dan secret acess key, pastikan untuk mendownload csv filenya.

## Configure Aws login from terminal

Install aws cli dan configure login

```bash
brew install awscli
# masukkan access key id dan secret access key
aws configure
```

Konfigurasi ini nanti akan digunakan oleh terraform untuk berkomunikasi ke AWS API. cara lain adalah dengan pass configfile setiap saat kita akan menjalankan perintah terraform.

## terraform provider dan resource

buat folder terraform-aws

```bash
mkdir terraform-aws
cd terraform-aws
git init .
touch provider.tf main.tf variables.tf outputs.tf
code .
```

isi main.tf:

```bash
# include terraform provider for aws
provider "aws" {
    profile = "default"
    region = "us-wes-2"
}

# create s3 bucket
resourse = "aws_s3_bucket" "tf_course" {
    bucket = "tf-course-20191118"
    acl = "private"
}
```

jangan lupa untuk commit ke git dan terraform init & terraform plan

```bash
git add .
git commit -m "add terraform provider and s3_bucket"
terraform init
terraform plan
```
