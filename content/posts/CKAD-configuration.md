---
title: "CKAD Configuration"
date: 2020-04-10T11:02:27+08:00
draft: false

categories: [kubernetes, CKAD]
tags: [kubernetes, CKAD]
---

Configuration (18%)

Practice questions based on these concepts

- Understand ConfigMaps
- Understand SecurityContexts
- Define an application’s resource requirements
- Create & Consume Secrets
- Understand ServiceAccounts