---
title: "Tools"
date: 2020-04-10T11:08:56+08:00
draft: false
---

Tools favorite:

- Kubernetes
- Google Cloud Platform (GCP)
- Amazon web services (AWS)
- Terraform (Infra as code)
- Helm (packaging and deploying k8s manifest)
- asdf (managing multiple version of application)
- git
- Docker
- Golang
- Elixir
- Python
- Jupyter notebook
- Visual studio code
- postgresql
- Elasticsearch
- Kafka
  
List diatas masih bisa lebih panjang misal saya juga suka ansible, saltstack, api gateway dan lain sebagainya.